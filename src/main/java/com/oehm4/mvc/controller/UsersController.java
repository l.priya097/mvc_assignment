package com.oehm4.mvc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.oehm4.mvc.dto.UsersDetails;
import com.oehm4.mvc.service.UsersService;
import com.oehm4.mvc.dto.RegisteredUsers;
import com.oehm4.mvc.dto.LoginDTO;

@Controller
@RequestMapping(value="/")
public class UsersController 
{
	@Autowired 
   private UsersService usersService;

	
	
	@RequestMapping(value = "/saveUsersDetails")
	public ModelAndView saveUsersDetails( UsersDetails usersDetails ,HttpServletRequest httpServletRequest)
	{   
		
		RegisteredUsers registeredUsers = (RegisteredUsers) httpServletRequest.getSession().getAttribute(" registeredUsers");
		
		usersService.saveUsersDetails(usersDetails);
		List<UsersDetails> list = usersService.getUsersDetails(registeredUsers.getId());
		return new ModelAndView("home.jsp", "list", list);
		
		
	}
	
    //_______________________________________________________________________________________________________________________\\
	
	@RequestMapping(value="/saveRegisteredUsersDetails")
	public ModelAndView saveRegisteredUsersDetails(RegisteredUsers registeredUsers )
	{
	  usersService.saveRegisteredUsersDetails(registeredUsers);
	  return new ModelAndView("login.jsp"," msg ","Registration successfull please login");
	}
	
	@RequestMapping(value="/login")
	public ModelAndView login(LoginDTO loginDTO ,HttpServletRequest httpServletRequest )
	{
		RegisteredUsers registeredUsers = usersService.getRegisteredUsersDetails(loginDTO);
		if( registeredUsers!=null)
		{
			HttpSession httpSession = httpServletRequest.getSession();
			httpSession.setAttribute("registeredUsers", registeredUsers);
			List<UsersDetails> list = usersService.getUsersDetails(registeredUsers.getId());
			list.forEach(a->{System.out.println(a);});
			  return new ModelAndView("home.jsp","list",list);
		  }
		  
		  return new ModelAndView("login.jsp","msg" , "invalid credentials !");
				  
	  
			
		}
			
	}
	

