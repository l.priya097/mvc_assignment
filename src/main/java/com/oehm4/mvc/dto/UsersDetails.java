package com.oehm4.mvc.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "UsersDetails")
public class UsersDetails implements Serializable
{   
	@Id
	@GenericGenerator(name = "UsersDetails_auto", strategy = "increment")
	@GeneratedValue(generator = "UsersDetails_auto")
	@Column(name = "id")
	private Long id;
	
	@Column(name = "AppName")
    private String appName;
	
	@Column(name = " UserName")
    private String userName;
	
	@Column(name = "ContactNumber")
    private Long contactNumber;
	
	@Column(name = "City")
    private String city;
	
	@Column(name = "PinCode")
    private Long pinCode;
	
	@Column(name = "Country")
    private String country;
	
	@Column(name = "Language")
    private String language;
	
	@Column(name = "Email")
    private String email;
	
	@Column(name = "Password")
    private String password;
  
  
  public UsersDetails() {}


public Long getId() {
	return id;
}


public void setId(Long id) {
	this.id = id;
}


public String getAppName() {
	return appName;
}


public void setAppName(String appName) {
	this.appName = appName;
}


public String getUserName() {
	return userName;
}


public void setUserName(String userName) {
	this.userName = userName;
}


public Long getContactNumber() {
	return contactNumber;
}


public void setContactNumber(Long contactNumber) {
	this.contactNumber = contactNumber;
}


public String getCity() {
	return city;
}


public void setCity(String city) {
	this.city = city;
}


public Long getPinCode() {
	return pinCode;
}


public void setPinCode(Long pinCode) {
	this.pinCode = pinCode;
}


public String getCountry() {
	return country;
}


public void setCountry(String country) {
	this.country = country;
}


public String getLanguage() {
	return language;
}


public void setLanguage(String language) {
	this.language = language;
}


public String getEmail() {
	return email;
}


public void setEmail(String email) {
	this.email = email;
}


public String getPassword() {
	return password;
}


public void setPassword(String password) {
	this.password = password;
}


@Override
public String toString() {
	return "UsersDetails [id=" + id + ", appName=" + appName + ", userName=" + userName + ", contactNumber="
			+ contactNumber + ", city=" + city + ", pinCode=" + pinCode + ", country=" + country + ", language="
			+ language + ", email=" + email + ", password=" + password + "]";
}
	

}
