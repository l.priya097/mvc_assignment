package com.oehm4.mvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oehm4.mvc.dao.UsersDAO;
import com.oehm4.mvc.dto.UsersDetails;
import com.oehm4.mvc.dto.RegisteredUsers;
import com.oehm4.mvc.dto.LoginDTO;

@Service
public class UsersService
{
	@Autowired
	private UsersDAO usersDAO;//<---Injecting the DAO class into Service Layer
	
//***********method for saving*********\\   
	
	
	public void saveUsersDetails(UsersDetails usersDetails) 
	{	
		usersDAO.saveUsersDetails(usersDetails);
	}
	
	
	//***********method for fetching*********\\   
	
	
	public List<UsersDetails> getUsersDetails(Long id) 
	{
		return usersDAO.getUsersDetails(id);
	}

	//_________________________________________________________________________________\\	
	
	
	
	//***********method for saving*********\\   
		
		
		public void saveRegisteredUsersDetails(RegisteredUsers registeredUsers ) 
		{	
			usersDAO.saveRegisteredUsersDetails(registeredUsers);
		}
		
		
		//***********method for fetching*********\\   
		
		
		public RegisteredUsers getRegisteredUsersDetails(LoginDTO loginDTO ) 
		{
			return usersDAO.getRegisteredUsersDetails(loginDTO);
		}
	
}
