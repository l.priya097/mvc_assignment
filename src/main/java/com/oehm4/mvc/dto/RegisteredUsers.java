package com.oehm4.mvc.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name ="RegisteredUsers" )
public class RegisteredUsers implements Serializable
{
	@Id
	@GenericGenerator(name = "RegisteredUsers_auto", strategy = "increment")
	@GeneratedValue(generator = "RegisteredUsers_auto")
	@Column(name = "id")
	private Long id;
	

	@Column(name = " UserName")
    private String userName;
	
	@Column(name = "ContactNumber")
    private Long contactNumber;
	
	
	@Column(name = "Email")
    private String email;
	
	@Column(name = "Password")
    private String password;
	
	
	public RegisteredUsers() {}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(Long contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "RegisteredUsers [id=" + id + ", userName=" + userName + ", contactNumber=" + contactNumber + ", email="
				+ email + ", password=" + password + "]";
	}
  
}
