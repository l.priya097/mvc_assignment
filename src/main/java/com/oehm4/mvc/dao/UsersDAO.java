package com.oehm4.mvc.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;



import com.oehm4.mvc.dto.UsersDetails;
import com.oehm4.mvc.dto.RegisteredUsers;
import com.oehm4.mvc.dto.LoginDTO;

@Repository
public class UsersDAO 
{
	@Autowired
	private SessionFactory sessionFactory;
	
    public void saveUsersDetails(UsersDetails usersDetails)
    {
    	
    	 Session session = sessionFactory.openSession(); 
	   try 
	   {
		   Transaction transaction = session.beginTransaction(); 
		   session.save(usersDetails);
		   transaction.commit();
	   }
	   catch (Exception e) {}
	   finally { session.close();}
		
	
     }
    
    public List<UsersDetails> getUsersDetails(Long id)
    {
    	Session session = sessionFactory.openSession();
    	String hql="from UsersDetails where register.id=:i";
    	Query query = session.createQuery(hql);
    	query.setParameter("i", id);
    	List<UsersDetails> list = query.list();
    	return list;
    } 
    
 //_______________________________________________________________________________\\   
    
    public void saveRegisteredUsersDetails(RegisteredUsers registeredUsers)
    {
    	
    	 Session session = sessionFactory.openSession(); 
	   try 
	   {
		   Transaction transaction = session.beginTransaction(); 
		   session.save(registeredUsers);
		   transaction.commit();
	   }
	   catch (Exception e) {}
	   finally { session.close();}
		
	
     }
    
    public RegisteredUsers getRegisteredUsersDetails(LoginDTO loginDTO )
    {
    	Session session = sessionFactory.openSession();
    	String hql="from RegisteredUsers where email=:e and password=:p";
    	Query query = session.createQuery(hql);
    	query.setParameter("e", loginDTO.getEmail());
    	query.setParameter("p", loginDTO.getPassword());
    	RegisteredUsers registeredUsers = (RegisteredUsers) query.uniqueResult();
    	return registeredUsers;
    } 
    
    
    
}
